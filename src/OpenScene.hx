import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;

using Lambda;

class OpenScene extends Sprite {

	public var onComplete:Void -> Void;
	
	public function new() {
		super();

    	var introHistory = new Dialogue(
			"Odin",
			["This saga begins in an age before the greats...",
		     "Before Leif Eriksson discovered Vinland around 1000...",
			 "- Columbus \"discovered\" North America 500 years later -",
			 "Before his father, Erik the Red, settled in Greenland...",
			 "- it is actually quite icy, Erik just wanted more settlers -",
			 "Before Ingolfur Arnarson established Reykjavik in 874",
			 "- Reykjavik means \"Cove of Smoke\" -",
			 "I digress."].list());

		var introWed = new Dialogue(
			"Odin",
			["This saga begins on a Wednesday...",
			"I am honored that this day is named after me...",
			"Since I'm also known as Woden, Wednesday is Woden's Day...",
			"I love knowledge, and I could talk endlessly...",
			"But let us start."].list());

		var introEnd = new Dialogue(
			"Odin",
			["The year is 793...",
			"You must keep your crew alive...",
			"And fight with honor."].list());

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(introHistory); 
			}, 0.75);
		addChild(introHistory);
       
		introHistory.onComplete = function() {
			removeChild(introHistory);
			Starling.juggler.remove(introHistory);
			addChild(introWed);
			Starling.juggler.add(introWed);
		}

		introWed.onComplete = function() {
			removeChild(introWed);
			Starling.juggler.remove(introWed);
			addChild(introEnd);
			Starling.juggler.add(introEnd);
		}
		
		introEnd.onComplete = function() { this.onComplete(); };
	}	

}
