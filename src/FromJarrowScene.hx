import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;

using Lambda;

class FromJarrowScene extends Sprite {
	
	public var onComplete:Void -> Void;

	public function new() {
		super();

		var d1 = new Dialogue(
			"Captain",
			["What an unfortunate day...",
		     "We were pushed back and had to retreat...",
			 "And a storm made retreating worse...",
			 "Yet, that is destiny...",
			 "We shall be successful from this point onward!"].list());

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 2.0);
		addChild(d1);
       
		d1.onComplete = function() { this.onComplete(); };

	}	

}
