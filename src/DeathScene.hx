import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;
import Crewman;

using Lambda;

class DeathScene extends Sprite {
    
    public var onComplete : Void -> Void;
	public var crewMember:Crewman;

	public function new(crew:Crewman) {
		super();
		crewMember = crew;
    	var d1 = new Dialogue(
			"Captain",
			["Today we honor " + crewMember.name + "...",
			 crewMember.name + " fought with courage and strength...",
			 "May the Valkyries be swift...",
			 "And may " + crewMember.name + " enjoy Valhalla until Ragnarok, the end times."].list());
        d1.onComplete = function(){this.onComplete();};

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 0.0);
		addChild(d1);
	}	

}
