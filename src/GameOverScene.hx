import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;

using Lambda;

class GameOverScene extends Sprite {

	public var onComplete:Void -> Void;
	
	public function new() {
		super();

    	var d1 = new Dialogue(
			"Odin",
			["And now the epic saga comes to an end...",
			 "Yggdrasil, the cosmological tree that holds the nine worlds",
			 "withers and falls as Ragnarok approaches...",
			 "This is Ginnungagap, nothing...",
			 "All that is will cease to be, including me."].list());

		var d2 = new Dialogue(
			"Odin",
			["Yet...",
			 "Time is cyclical, all that is will begin anew...",
			 "The heavens, the gods, and even you...",
			 "All life comes from death, it's worth learning...",
			 "Now set forth, again, young viking!"].list()); 


		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 0.75);
		addChild(d1);
       
		d1.onComplete = function() {
			removeChild(d1);
			Starling.juggler.remove(d1);
			addChild(d2);
			Starling.juggler.add(d2);
		}

		d2.onComplete = function() { this.onComplete(); };
	}	

}
