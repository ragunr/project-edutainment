import starling.display.*;
import starling.textures.*;

class ScrollImage extends Sprite {
    private var images : Array<Image>;
    public var scrollX : Float = 0;

    public function new(tex:Texture){
        super();
        images = new Array<Image>();
        for(i in 0...2)
        {
            var image = new Image(tex);
            image.smoothing = TextureSmoothing.NONE;
            addChild(image);
            image.x = image.width*i;
            images.push(image);
        }
    }

    public function scroll(x:Float){
        scrollTo(scrollX+x);
    }

    public function scrollTo(x:Float)
    {
        x %= 1;
        scrollX = x;
        for(i in 0...2)
        {
            images[i].x = Std.int((-x+i)*images[i].width);
        }

    }
}

