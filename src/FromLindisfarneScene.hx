import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;
import Crewman;

using Lambda;

class FromLindisfarneScene extends Sprite {

	public var onComplete:Void -> Void;
	public var crewMember:Crewman;

	public function new(crew:Crewman) {
		super();
		crewMember = crew;

    	var d1 = new Dialogue(
			"Captain",
			["We fought valiantly,",
			 "King Aethelred could do nothing to save Lindisfarne."].list());

        var d2 = new Dialogue(
			crewMember.name,
			["The monastery was destined to fall.",
			 "Also...",
			 "I found these neat horns,",
			 "If I could just make a helmet..."].list());

		var d3 = new Dialogue(
			"Captain",
			["A helmet with horns?",
			 "Oh " + crewMember.name + ", Don't even think about that!",
			 "We vikings have no use for horned-helmets..."].list());

		var d4 = new Dialogue(
			crewMember.name,
			["I... I was just... nevermind."].list());

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 0.75);
		addChild(d1);
       
		d1.onComplete = function() {
			removeChild(d1);
			Starling.juggler.remove(d1);
			addChild(d2);
			Starling.juggler.add(d2);
		}

		d2.onComplete = function() {
			removeChild(d2);
			Starling.juggler.remove(d2);
			addChild(d3);
			Starling.juggler.add(d3);
		}

		d3.onComplete = function() {
			removeChild(d3);
			Starling.juggler.remove(d3);
			addChild(d4);
			Starling.juggler.add(d4);
	    }	

		d4.onComplete = function() { this.onComplete(); };

	}	

}
