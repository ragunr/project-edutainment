import starling.display.Sprite;
import starling.display.Button;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Button;
import starling.text.TextField;
import starling.events.Event;
import starling.core.Starling;
import starling.textures.TextureSmoothing;
import Root;
import Game;
import OpenScene;

class Menu extends Sprite{
	
	private var bgColor:Quad;
	private var bgImage:Image;
	private var contentImage:Image;
	private var titleImage:Image;
	private var playButton:Button;
	private var instructionButton:Button;
	private var creditsButton:Button;
	private var backButton:Button;
	
	private var member1:TextField;
	private var member2:TextField;
	private var member3:TextField;
	private var member4:TextField;
	private var member5:TextField;
	private var member6:TextField;
	
	private var instructionText:TextField;
	
	private var menuRoot:Root;
	
	public function new(testRoot){
		super();
		this.menuRoot = testRoot;
		
		bgColor = new Quad(1024, 576, 0x000026);
		addChild(bgColor);
		
		bgImage = new Image(Root.assets.getTexture("background_img"));
                bgImage.scaleX = bgImage.scaleY = 2;
                bgImage.smoothing = TextureSmoothing.NONE;
		addChild(bgImage);
		
		contentImage = new Image(Root.assets.getTexture("content_img"));
		contentImage.x = 300;
		contentImage.y = 50;
		addChild(contentImage);
		
		titleImage = new Image(Root.assets.getTexture("title_img"));
		titleImage.x = (1024/2) - 275;
		titleImage.y = 125;
		addChild(titleImage);
			
		playButton = new Button(Root.assets.getTexture("button_texture"), "Play");
		playButton.x = 341 - 200;
		playButton.y = 475;
		playButton.fontName = "Gameboy_32_0";
		playButton.fontSize = 16;
		playButton.addEventListener(Event.TRIGGERED, onPlayButtonTriggered);
		addChild(playButton);
		
		instructionButton = new Button(Root.assets.getTexture("button_texture"), "Instructions");
		instructionButton.x = 341 * 2 - 260;
		instructionButton.y = 475;
		instructionButton.fontName = "Gameboy_32_0";
		instructionButton.fontSize = 16;
		instructionButton.addEventListener(Event.TRIGGERED, onInstructionButtonTriggered);
		addChild(instructionButton);
		
		creditsButton = new Button(Root.assets.getTexture("button_texture"), "Credits");
		creditsButton.x = 341 * 3 - 315;
		creditsButton.y = 475;
		creditsButton.fontName = "Gameboy_32_0";
		creditsButton.fontSize = 16;
		creditsButton.addEventListener(Event.TRIGGERED, onCreditsButtonTriggered);
		addChild(creditsButton);
		
		backButton = new Button(Root.assets.getTexture("button_texture"), "Back");
		backButton.x = 0;
		backButton.y = 0;
		backButton.fontName = "Gameboy_32_0";
		backButton.fontSize = 16;
		backButton.addEventListener(Event.TRIGGERED, onBackButtonTriggered);
	}
	
	public function onPlayButtonTriggered(event:Event){
		removeChild(titleImage);
		removeChild(contentImage);
		removeChild(playButton);
		removeChild(instructionButton);
		removeChild(creditsButton);
		removeChild(backButton);

		if(instructionText != null) instructionText.removeFromParent(true);
		if(member1 != null) member1.removeFromParent(true);
		if(member2 != null) member2.removeFromParent(true);
		if(member3 != null) member3.removeFromParent(true);
		if(member4 != null) member4.removeFromParent(true);
		if(member5 != null) member5.removeFromParent(true);
		if(member6 != null) member6.removeFromParent(true);

		var odinScene = new OpenScene();
		var odinSound = Root.assets.playSound("odin", 0, 9999);
		addChild(odinScene);
		odinScene.onComplete = function() {
			removeChild(odinScene);
			var game = new Game(menuRoot);
			addChild(game);
			odinSound.stop();
			Starling.juggler.add(game);
		}
	}
	
	public function onInstructionButtonTriggered(event:Event){
		removeChild(titleImage);
		addChild(backButton);
		
		if(member1 != null) member1.removeFromParent(true);
		if(member2 != null) member2.removeFromParent(true);
		if(member3 != null) member3.removeFromParent(true);
		if(member4 != null) member4.removeFromParent(true);
		if(member5 != null) member5.removeFromParent(true);
		if(member6 != null) member6.removeFromParent(true);
		
		instructionText = new TextField(300, 300, "Survive the perilous sea as a viking and learn more about sea faring survival.", "Gameboy_32_0");
		instructionText.fontSize = 32;
		instructionText.color = 0x21007F;
		instructionText.x = (1024/2) - 175;
		instructionText.y = 50;
		
		addChild(instructionText);
		
		instructionButton.removeEventListener(Event.TRIGGERED, onInstructionButtonTriggered);
		creditsButton.addEventListener(Event.TRIGGERED, onCreditsButtonTriggered);
	}
	
	public function onCreditsButtonTriggered(event:Event){
		removeChild(titleImage);
		removeChild(instructionText);
		addChild(backButton);
		
		if(instructionText != null) instructionText.removeFromParent(true);
		
		member1 = new TextField(300, 50, "Justin Liddicoat", "Gameboy_32_0");
		member2 = new TextField(300, 50, "Richard Lester", "Gameboy_32_0");
		member3 = new TextField(300, 50, "Nico Bedford", "Gameboy_32_0");
		member4 = new TextField(300, 50, "Ryan Buckingham", "Gameboy_32_0");
		member5 = new TextField(300, 50, "Ian Humphrey", "Gameboy_32_0");
		member6 = new TextField(300, 50, "Tanner Stevens", "Gameboy_32_0");
		
		member1.color = 0x21007F;
		member2.color = 0x21007F;
		member3.color = 0x21007F;
		member4.color = 0x21007F;
		member5.color = 0x21007F;
		member6.color = 0x21007F;
		
		member1.fontSize = 24;
		member2.fontSize = 24;
		member3.fontSize = 24;
		member4.fontSize = 24;
		member5.fontSize = 24;
		member6.fontSize = 24;
		
		member1.bold = true;
		member2.bold = true;
		member3.bold = true;
		member4.bold = true;
		member5.bold = true;
		member6.bold = true;

		member1.x = (1024/2) - 150;
		member1.y = 100;
		addChild(member1);
		
		member2.x = (1024/2) - 150;
		member2.y = 125;
		addChild(member2);
		
		member3.x = (1024/2) - 150;
		member3.y = 150;
		addChild(member3);
		
		member4.x = (1024/2) - 150;
		member4.y = 175;
		addChild(member4);
		
		member5.x = (1024/2) - 150;
		member5.y = 200;
		addChild(member5);
		
		member6.x = (1024/2) - 150;
		member6.y = 225;
		addChild(member6);
		
		creditsButton.removeEventListener(Event.TRIGGERED, onCreditsButtonTriggered);
		instructionButton.addEventListener(Event.TRIGGERED, onInstructionButtonTriggered);
	}
	
	public function onBackButtonTriggered(event:Event){
		addChild(titleImage);
		removeChild(backButton);
		
		if(instructionText != null) instructionText.removeFromParent(true);
		
		if(member1 != null) member1.removeFromParent(true);
		if(member2 != null) member2.removeFromParent(true);
		if(member3 != null) member3.removeFromParent(true);
		if(member4 != null) member4.removeFromParent(true);
		if(member5 != null) member5.removeFromParent(true);
		if(member6 != null) member6.removeFromParent(true);
		
		instructionButton.addEventListener(Event.TRIGGERED, onInstructionButtonTriggered);
		creditsButton.addEventListener(Event.TRIGGERED, onCreditsButtonTriggered);
	}
}
