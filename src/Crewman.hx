import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.utils.*;
import starling.textures.*;
import Math;
import Game;
import Overwatch;
using Lambda;

class Crewman implements IAnimatable{

    public static var NAMELIST = {
        ["Arne", "Bjorn", "Eirik", "Geir", "Gisle", "Gunnar", "Harald",
        "Hakon", "Inge", "Ivar", "Knut", "Leif", "Magnus", "Olav", 
        "Rolf", "Sigurd", "Snorre", "Steinar", "Orstein", "Trygve",
        "Ulf", "Vlademar"];
    }

    public static var STAT_DELTAS = {
        [FATIGUE=>0.01,
        HUNGER=>0.008,
        AGRO=>0.01,
        INJURY=>0,
        REBEL=>0.001];
    }

    public static var FOOD_JELOUSY : Float = -0.1;

    public var name : String;

    public var stats : Map<Stat,Float>;
    public var rapport : Map<Crewman,Float>;

    public var alive : Bool = true;
    public var activity : Activity = WAIT;
    public var node_name : String;

    public var game : Game;
    public var actor : Actor;
    public var juggler : Juggler;

    public function new(game:Game,namelist:Array<String>) {
        this.game = game;
        juggler = new Juggler();

        name = namelist[Std.random(namelist.length)];
        namelist.remove(name);

        stats = new Map<Stat,Float>();
        stats.set(Stat.FATIGUE, Math.random()*0.05);
        stats.set(Stat.HUNGER, Math.random()*0.05);
        stats.set(Stat.INJURY, Math.random()*0.05);
        stats.set(Stat.AGRO, Math.random()*0.005);
        stats.set(Stat.REBEL, Math.random()*0.25);


        rapport = new Map<Crewman,Float>();

        ready_emote();
    }

    public function setActor(actor:Actor) {
        this.actor = actor;
        juggler.add(actor);

        actor.addEventListener(TouchEvent.TOUCH,function(e:TouchEvent) {
                var touch = e.getTouch(actor,TouchPhase.ENDED);
                if(touch != null) trigger();
        });
    }

    public function trigger() {
        if(!alive) return;
        game.select_crewman(this);
    }

    public function advanceTime(t:Float) {
        juggler.advanceTime(t);

        if(!alive) return;


        for(stat in stats.keys())
            stats.set(stat,stats.get(stat)+STAT_DELTAS.get(stat)*t);

        if(activity == SLEEP) {
            stats.set(FATIGUE,stats.get(FATIGUE) - 0.06*t);
            stats.set(HUNGER,stats.get(HUNGER) + STAT_DELTAS.get(HUNGER)*-0.5*t);
        } 
        else if(activity == PASSEDOUT) {
            stats.set(FATIGUE,stats.get(FATIGUE) - 0.03*t);
            stats.set(HUNGER,stats.get(HUNGER) + STAT_DELTAS.get(HUNGER)*-0.2*t);
        }

        if(stats.get(HUNGER) >= 1 || stats.get(INJURY) >= 1) die();
        if(stats.get(FATIGUE) >= 1) passout();

        for(stat in stats.keys()) 
            stats.set(stat,Math.max(Math.min(stats.get(stat),1),0));
        for(key in rapport.keys())
            rapport.set(key,Math.max(Math.min(rapport.get(key),1),0));
    }

    public function die() {
        change_activity(WAIT);
        actor.removeFromParent();
        if(node_name != null) 
            game.overwatch.getNode(node_name).removeActor(actor);
        alive = false;
        game.deathOf(this);
    }

    public function passout() {
        change_activity(PASSEDOUT);
        actor.resting();
        game.overwatch.cancelTraversal(actor);
    }

    public function change_activity(new_activity:Activity) {
        if(!alive) return;
        if(activity == ROW) {
            game.stop_animate_oar(node_name);
        }
    
        activity = new_activity;
    }

    public function feed(target:Crewman, food:Float) {
        if(!alive) return;
        if(target == this) {
            stats.set(HUNGER,stats.get(HUNGER) - food);
            var jelousy = food*FOOD_JELOUSY;
            for(key in rapport.keys()){
                rapport.set(key,rapport.get(key)-jelousy);
            }

        } else {
            var jelousy = food*FOOD_JELOUSY/rapport.list().length;
            for(key in rapport.keys()){
                rapport.set(key,rapport.get(key)+jelousy);
            }
        }
    }

    public function initializeRapport(crew:Array<Crewman>){
        for(crewman in crew) {
            if(crewman != this) rapport.set(crewman,0.5);
        }
    }

    public function getRapport(crewman:Crewman) {
        return rapport.get(crewman);
    }

    public function ready_emote() {
        if(!alive) return;
        juggler.delayCall(function () {
                emote();
                ready_emote();
                },7+Math.random()*6);
    }

    public function emote() {
        if(!alive) return;

        var stat_total = Lambda.fold(stats,function(a,b){return a+b;},0);
        var r = stat_total * Math.random();
        var worst_stat = null;
        for(stat in stats.keys()) {
            worst_stat = stat;
            r -= stats[stat];
            if(r <= 0) break;
        }

        var intensity = Std.int((stats.get(worst_stat))*4);

        var e = new Emote(worst_stat, intensity, this);
        e.x = 100;
        e.y = -50;
        actor.addChild(e);
    }

    public function trace_state() {
        trace("---------------------");
        if(!alive) {
            trace("dead");
        } else {
            trace("Name: "+name);
            trace("Fatigue : "+stats.get(FATIGUE));
            trace("Hunger : "+stats.get(HUNGER));
            trace("Agro : "+stats.get(AGRO));
            trace("Injury : "+stats.get(INJURY));
            trace("Rebel : "+stats.get(REBEL));
        }
    }
}

enum Stat {
    FATIGUE;
    HUNGER;
    INJURY;
    AGRO;
    REBEL;
}

enum Activity {
    WAIT;
    ROW;
    EAT;
    SLEEP;
    PASSEDOUT;
}

class Emote extends Sprite {
    public function new(stat : Stat, intensity : Int, crewman:Crewman){
        super();
        var tex : String = null;
        if(stat == HUNGER && intensity == 1) tex = "emote_think_food";
        else if(stat == HUNGER && intensity == 2) tex = "emote_say_food";
        else if(stat == HUNGER && intensity == 3) tex = "emote_yell_food";
        else if(stat == FATIGUE && intensity == 1) tex = "emote_think_sleep";
        else if(stat == FATIGUE && intensity == 2) tex = "emote_say_sleep";
        else if(stat == FATIGUE && intensity == 3) tex = "emote_yell_sleep";

        if(tex != null) {
            var img = new Image(Root.assets.getTexture(tex));
            img.smoothing = TextureSmoothing.NONE;
            img.scaleX = 4;
            img.scaleY = 4;
            img.readjustSize();
            addChild(img);
        }

        alignPivot();

        var juggler = crewman.juggler;
        juggler.delayCall(function (){
                parent.removeChild(this);
                dispose();
                },5.0);
    }
}
