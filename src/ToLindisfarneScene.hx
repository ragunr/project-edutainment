import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;

using Lambda;

class ToLindisfarneScene extends Sprite {
	
	public var onComplete:Void -> Void;

	public function new() {
		super();

    	var d1 = new Dialogue(
			"Captain",
			["Men and Women, we depart from Norway in search of plunder...",
			 "In search of more land to settle...",
			 "To use these resources to establish trade...",
			 "To protect our culture from Charlemagne...",
			 "He's conquered and converted the Saxons already."].list());

        var d2 = new Dialogue(
			"Crew",
			["What a fine day for travel, Captain!"].list());

		var d3 = new Dialogue(
			"Captain",
			["The weather is perfect, I would allow no less perfection for travel...",
			"We head due East, towards Northumbria to plunder...",
			"May we fight with honor, we are not thieves...",
			"And may the gods protect us."].list());

		var d4 = new Dialogue(
			"Crew",
			["Let's go a-viking!"].list());

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 0.75);
		addChild(d1);
       
		d1.onComplete = function() {
			removeChild(d1);
			Starling.juggler.remove(d1);
			addChild(d2);
			Starling.juggler.add(d2);
		}

		d2.onComplete = function() {
			removeChild(d2);
			Starling.juggler.remove(d2);
			addChild(d3);
			Starling.juggler.add(d3);
		}

		d3.onComplete = function() {
			removeChild(d3);
			Starling.juggler.remove(d3);
			addChild(d4);
			Starling.juggler.add(d4);
	    }	

		d4.onComplete = function() { this.onComplete(); };

	}	

}
