import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.utils.*;
import starling.textures.*;
import Root;
import Math;
import Crewman;
import Overwatch;
import ScrollImage;
import DeathScene;
import FromLindisfarneScene;
import FromJarrowScene;
import ToLindisfarneScene;
import ToJarrowScene;
import EndScene;
using Lambda;

class Game extends Sprite implements IAnimatable {
    public static inline var CREW_SIZE = 10;
    public static inline var FOOD_FACTOR = 1.0;
    public static inline var MEAL_SIZE = 0.25;

    public static inline var BASE_SPEED = 1.0;
    public static inline var SYN_SPEED_BONUS = 0.3;
    public static inline var WIND_SPEED_BONUS = 0.3;

    // Layers
    private var background_layer : Sprite;
    private var crew_layer : Sprite;
    private var oar_layer : Sprite;
    private var ui_layer: Sprite;

    public var crew : Array<Crewman>;
    public var selected_crewman : Crewman;
    public var clock : Float = 0;
    public var ship_speed : Float = 0;
    public var travel_distance : Float = 0;
    public var dest_distance : Float = 0;
    public var pause = 0;
	public var current_level = 0;
	public var current_sound = null;
	public var ocean_sound = null;

    var food : Int = 0;

    public var juggler : Juggler;
    public var overwatch : Overwatch;
    private var select_listeners : List<Crewman->Void>;

    var selected_highlight : Image;

    var coast : ScrollImage;
    var oars : Map<String,Sprite3D>;
    var hud : Hud;
	
	var nRoot:Root;
    public function new(mRoot:Root) {
        super();
		this.nRoot = mRoot;
        juggler = new Juggler();
        select_listeners = new List<Crewman->Void>();

        // Setup layers
        background_layer = new Sprite();
        crew_layer = new Sprite();
        oar_layer = new Sprite();
        ui_layer = new Sprite();

        addChild(background_layer);
        addChild(crew_layer);
        addChild(oar_layer);
        addChild(ui_layer);

        // Initialize crew
        crew = new Array<Crewman>();
        var namelist = Crewman.NAMELIST.slice(0);
        for(i in 0...CREW_SIZE)
            crew.push(new Crewman(this,namelist));

        // Setup background elements
        var sky = new Quad(Root.WIDTH,Root.HEIGHT/3,0x5fcde4);
        var water = new Quad(Root.WIDTH,Root.HEIGHT*2/3,0x639bff);
        water.y = Root.HEIGHT/3;
        coast = new ScrollImage(Root.assets.getTexture("coast"));
        coast.scaleX = coast.scaleY = 2;
        coast.y = -50;
        var boat = new Image(Root.assets.getTexture("full_ship"));
        boat.scaleX = boat.scaleY = 2;
        boat.smoothing = TextureSmoothing.NONE;
        boat.y = -25;
        var sail = new MovieClip(Root.assets.getTextures("sail_f"),0.25);
        sail.scaleX = sail.scaleY = 2;
        sail.smoothing = TextureSmoothing.NONE;
        sail.y = -25;
        juggler.add(sail);
        var wake = new MovieClip(Root.assets.getTextures("wake_f"),2);
        wake.scaleX = wake.scaleY = 2;
        wake.smoothing = TextureSmoothing.NONE;
        wake.y = -25;
        juggler.add(wake);

        background_layer.addChild(sky);
        background_layer.addChild(water);
        background_layer.addChild(coast);
        background_layer.addChild(boat);
        background_layer.addChild(sail);
        background_layer.addChild(wake);

        // Setup highlight
        selected_highlight = new Image(Root.assets.getTexture("highlight"));
        selected_highlight.smoothing = TextureSmoothing.NONE;
        selected_highlight.scaleX = 4;
        selected_highlight.scaleY = 4;
        selected_highlight.readjustSize();
        selected_highlight.alignPivot();
        //selected_highlight.alpha = 0.5;
        selected_highlight.pivotY += 8;

        //Builds the crew and ui buttons
        initialize_scene();

        hud = new Hud(this);
        ui_layer.addChild(hud);

        // Setup starting game rules
        dest_distance = 70;
        var min_time = dest_distance/BASE_SPEED;
        var food_delta = Crewman.STAT_DELTAS.get(HUNGER)*min_time*CREW_SIZE;
        food = Std.int(food_delta/MEAL_SIZE*FOOD_FACTOR);
        //trace("Starting food: "+food);
        hud.setMealCount(food);
        hud.setShipSpeed(ship_speed);
        hud.setShipProgress(travel_distance);

		ocean_sound = Root.assets.playSound("ocean", 0, 9999);
		current_sound = Root.assets.playSound("viking", 0, 9999);
    }

    private static var NODES : Array<Array<Dynamic>> = {
        var center_x = Root.WIDTH/2;
        var left_x = 80;
        var right_x = Root.WIDTH-80;
        var center_button_x = center_x;
        var left_button_x = left_x + 50;
        var right_button_x = right_x-125;
        var gap = 40;
        [["Row01", Activity.ROW, center_x+gap*-5.5, 400,center_button_x],
        ["Row02", Activity.ROW,  center_x+gap*-4.5, 400,center_button_x],
        ["Row03", Activity.ROW,  center_x+gap*-3.5, 400,center_button_x],
        ["Row04", Activity.ROW,  center_x+gap*-2.5, 400,center_button_x],
        ["Row05", Activity.ROW,  center_x+gap*-1.5, 400,center_button_x],
        ["Row06", Activity.ROW,  center_x+gap*-0.5, 400,center_button_x],
        ["Row07", Activity.ROW,  center_x+gap*0.5, 400,center_button_x],
        ["Row08", Activity.ROW,  center_x+gap*1.5, 400,center_button_x],
        ["Row09", Activity.ROW,  center_x+gap*2.5, 400,center_button_x],
        ["Row10", Activity.ROW,  center_x+gap*3.5, 400,center_button_x],
        ["Food1", Activity.EAT,  left_x+gap*0, 400,left_button_x],
        ["Food2", Activity.EAT,  left_x+gap*1, 400,left_button_x],
        ["Food3", Activity.EAT,  left_x+gap*2, 400,left_button_x],
        ["Bed1", Activity.SLEEP, right_x-gap*0, 400,right_button_x],
        ["Bed2", Activity.SLEEP, right_x-gap*1, 400,right_button_x],
        ["Bed3", Activity.SLEEP, right_x-gap*2, 400,right_button_x],
        ["Bed4", Activity.SLEEP, right_x-gap*3, 400,right_button_x],
        ["Bed5", Activity.SLEEP, right_x-gap*4, 400,right_button_x]];
    }

    private function initialize_scene(){
        overwatch = new Overwatch();
        oars = new Map<String, Sprite3D>();

        //var cur_x = 50;
        for(node in NODES){
            overwatch.addNode(node[0], node[2], node[3]);

            // Setup mark to show where node is
            /*var node_quad = new Quad(2,2,0x000000);
            node_quad.alignPivot();
            node_quad.x = node[2];
            node_quad.y = node[3];
            ui_layer.addChild(node_quad);*/

            // Setup action button for the node
            var button = new ActionButton(this,node[0],node[1]);
            button.alignPivot();
            button.x = node[4];
            button.y = node[3]-125;
            ui_layer.addChild(button);

            // If its a row node add an oar
            if(node[1] == ROW){
                var oar_img = new Image(Root.assets.getTexture("oar"));
                oar_img.smoothing = TextureSmoothing.NONE;
                oar_img.scaleX = oar_img.scaleY = 2;
                oar_img.readjustSize();
                oar_img.rotation = Math.PI/4;
                oar_img.touchable = false;
                var oar = new Sprite3D();
                oar.pivotX = 0;
                oar.pivotY = 0;
                oar.x = node[2]-8;
                oar.y = node[3]-25;
                oar.scaleY = 1.3;
                oars.set(node[0],oar);

                oar.addChild(oar_img);
                oar_layer.addChild(oar);
            }

            //cur_x += 100;
        }

        for(i in 0...crew.length) {
            var crewman = crew[i];

            var actor = new Actor();
            actor.scaleX = 0.5;
            actor.scaleY = 0.5;
            actor.alignPivot(HAlign.CENTER, VAlign.BOTTOM);
            crew_layer.addChild(actor);
            crewman.setActor(actor);

            //overwatch.queueTravelTo(actor, NODES[i][0]);
            action_row(crewman,NODES[i][0]);
        }

		travel_prompt();

        overwatch.advanceTravels();
    }

    public function select_crewman(crewman:Crewman) {
        selected_crewman = crewman;
        selected_highlight.x = crewman.actor.pivotX;
        selected_highlight.y = crewman.actor.pivotY-50;
        crewman.actor.addChildAt(selected_highlight,0);
        crew_layer.setChildIndex(crewman.actor,0);

        for(listener in select_listeners){
            listener(crewman);
        }

        //crewman.trace_state();
    }

    public function addSelectionListener(callback:Crewman->Void) {
        select_listeners.add(callback);
    }

    public function removeSelectionListener(callback:Crewman->Void) {
        select_listeners.remove(callback);
    }

    public function advanceTime(t:Float){
        if(pause > 0) return;
        clock += t;

        juggler.advanceTime(t);
        overwatch.advanceTravels(t);

        if(selected_crewman!=null)hud.updateCharacter(selected_crewman);
        var crewmen_rowing = 0;
        for(crewman in crew) {
            if(!crewman.alive) continue;
            crewman.advanceTime(t);
            //crewman.trace_state();
            if(crewman.activity == ROW) crewmen_rowing += 1;
        }
        ship_speed = BASE_SPEED*crewmen_rowing/crew.length;
        ship_speed += WIND_SPEED_BONUS;
        if(crewmen_rowing == crew.length) ship_speed += SYN_SPEED_BONUS;
        hud.setShipSpeed(ship_speed);

        travel_distance += ship_speed*t;
        coast.scroll(ship_speed*t*0.01);
        hud.setShipProgress(travel_distance);
        if(travel_distance > dest_distance) dest_reached();

        /*if(count_live_crew() <= 0) {
			var gameover = new GameOverScene();
			pause += 1;
			addChild(gameover);
			gameover.onComplete = function() {
				removeChild(gameover);
				pause -= 1;
            	nRoot.start();
			}
            //gameover();
        }*/
    }

    public function action_feed(crewman:Crewman,node:String) {
        if(crewman == null) return;
        crewman.actor.walk();
        overwatch.queueTravelTo(crewman.actor,node,function(){
                    crewman.actor.eat();
                    crewman.change_activity(Activity.EAT);
                    feed(crewman, MEAL_SIZE);
                    //trace("fed");
                });
    }

    public function action_row(crewman:Crewman,node:String) {
        if(crewman == null) return;
        crewman.actor.walk();
        overwatch.queueTravelTo(crewman.actor, node, function() {
                    crewman.actor.row();
                    crewman.change_activity(Activity.ROW);
                    crewman.node_name = node;
                    start_animate_oar(node);
                    //trace("row");
                });
    }

    public function action_sleep(crewman:Crewman,node:String) {
        if(crewman == null) return;
        crewman.actor.walk();
        overwatch.queueTravelTo(crewman.actor, node, function() {
                    crewman.actor.resting();
                    crewman.change_activity(Activity.SLEEP);
                    //trace("sleep");
                });
    }

    public function feed(target:Crewman, food:Float){
        if(this.food <= 0) {
            //trace("no food left.");
            return;
        }
        for(crewman in crew){
            crewman.feed(target,food);
        }
        this.food -= 1;
        //trace("Meals left: "+this.food);
        hud.setMealCount(this.food);
    }

    private function dest_reached() {
        travel_distance = 0;
		current_level += 1;
		travel_prompt();
        //trace("Level Complete");
    }

	private function travel_prompt() {
		var scene:Dynamic = null;
		var scene2:Dynamic = null;
		if (current_level == 0) {
			scene = new ToLindisfarneScene();
		}
		else if (current_level == 1) {
			scene = new FromLindisfarneScene(crew[Std.random(crew.length)]);
			scene2 = new ToJarrowScene();
		}
		else if (current_level == 2) {
			scene = new FromJarrowScene();
			scene2 = new EndScene();
		}
		pause += 1;
		addChild(scene);
		scene.onComplete = function() {
			removeChild(scene);
			if (scene2) {
				pause += 1;
				addChild(scene2);
				if (current_level == 2) {
					if (current_sound != null) current_sound.stop();
					ocean_sound.stop();
					current_sound = Root.assets.playSound("odin", 0, 9999);
				}
				scene2.onComplete = function() {
					pause -= 1;
					if (current_level == 2) current_sound.stop();
					removeChild(scene2);
					if (current_level == 2) {
						nRoot.reset();
					}
				}
			}
			pause -= 1;
		}
	}

    public function start_animate_oar(node_name:String){
        var oar = oars.get(node_name);
        oar.rotationX = -Math.PI*1.6/4;
        oar.rotationZ = 0;
        var row_tween = new Tween(oar,3,Transitions.LINEAR);
        row_tween.animate("rotationZ",Math.PI*2);
        row_tween.repeatCount = 0;
        juggler.add(row_tween);
        row_tween.advanceTime(clock%3);
    }

    public function stop_animate_oar(node_name:String){
        var oar = oars.get(node_name);
        oar.rotationX = -Math.PI*1.6/4;
        oar.rotationZ = Math.PI*1/4;
        juggler.removeTweens(oar);
    }

    public function deathOf(crewman : Crewman){
        var death_scene = new DeathScene(crewman);
        addChild(death_scene);
		Root.assets.playSound("die", 0, 1);
        pause += 1;
        death_scene.onComplete=function(){
            removeChild(death_scene);		
        	if(count_live_crew() <= 0) {
				var gameover = new GameOverScene();
				addChild(gameover);
				if (current_sound != null) current_sound.stop();
				ocean_sound.stop();
				current_sound = Root.assets.playSound("odin", 1, 999);
				pause += 1;
				gameover.onComplete = function() {
					removeChild(gameover);
					current_sound.stop();
					pause -= 1;
            		nRoot.start();
				}
        	}
			pause -= 1;
		}
    }


    public function count_live_crew(){
        var live = 0;
        for(crewman in crew)
            if(crewman.alive)
                live += 1;
        return live;
    }
}

class ActionButton extends Sprite implements IAnimatable {

    var game : Game;
    var node : String;
    var activity : Activity;

    var state : ButtonState;

    public function new(game:Game,node:String,activity:Activity){
        super();
        this.game = game;
        this.node = node;
        this.activity = activity;

        var tex : String = null;
        var c : Int = 0;
        if(activity == Activity.ROW){
            tex = "row_button";
            c = 0xC04040;
        }
        else if(activity == Activity.EAT) {
            tex = "eat_button";
            c = 0x40C040;
        }
        else if(activity == Activity.SLEEP) {
            tex = "sleep_button";
            c = 0x4040C0;
        }

        //var quad = new Quad(50,50,0x445500);
        var quad = new Image(Root.assets.getTexture(tex));
        quad.smoothing = TextureSmoothing.NONE;
        quad.scaleX = quad.scaleY = 2;
        quad.color = c;
        addChild(quad);

        addEventListener(TouchEvent.TOUCH, function(e:TouchEvent){
                var touch = e.getTouch(this,TouchPhase.ENDED);
                if(touch != null) trigger();
                });

        game.addSelectionListener(on_selection);

        setState(ButtonState.INVALID);
    }

    public function trigger(?crewman:Crewman){
        if(state == INVALID) return;
        if(crewman == null) crewman = game.selected_crewman;
        //trace(crewman);

        crewman.change_activity(WAIT);
        crewman.node_name = null;
        //trace(activity);
        if(activity == EAT) {
            game.action_feed(crewman,node);
        }
        else if(activity == ROW) {
            game.action_row(crewman,node);
        }
        else if(activity == SLEEP) {
            game.action_sleep(crewman,node);
        }
        else {
        }

        on_selection(crewman);
    }

    public function on_selection(crewman : Crewman):Void{
        var ow_node = game.overwatch.getNode(node);
        if(ow_node.isEmpty() || ow_node.containsActor(crewman.actor)) setState(ButtonState.READY);
        else setState(ButtonState.INVALID);
    }

    public function setState(state:ButtonState){
        this.state = state;
        if(state == READY){
            alpha = 1.0;
            touchable = true;
        }
        else if(state == INVALID) {
            alpha = 0.0;
            touchable = false;
        }

    }


    public function advanceTime(t:Float){
    }
}

enum ButtonState {
    READY;
    INVALID;
}
