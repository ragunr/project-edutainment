import starling.display.Sprite;
import starling.display.Image;
import starling.display.MovieClip;
import starling.animation.*;
import starling.textures.TextureSmoothing;
import starling.utils.Color;
using Lambda;

class Overwatch extends Sprite{
	public var actors:Map<Actor,Node>;
	private var nodes:Map<String,Node>;
	public var juggler:Juggler;
	private var traversals:Map<Actor, Traversal>;

	public function new(){
		actors = new Map<Actor, Node>();
		nodes = new Map<String,Node>();
		juggler = new Juggler();
		traversals = new Map<Actor, Traversal>();
		super();
	}

	public function addNode(node_label:String,x:Int,y:Int){
		nodes[node_label]=new Node(x,y);
	}
	public function getNode(node_label){return nodes[node_label];}

	public function queueTravelTo(actor, destinationNode_label, ?onComplete:Void->Void=null){
		var destination = getNode(destinationNode_label);
		if(destination!=null){
			if(actors[actor]!=null)actors[actor].removeActor(actor);
			var path = new Traversal(this, actor, destination, onComplete);
			if(traversals[actor]!=null)juggler.remove(traversals[actor]);
			traversals[actor]=path;
			juggler.add(path);
		}
	}
	public function advanceTravels(t:Float=-1){
		juggler.advanceTime(t);
	}
        public function cancelTraversal(actor:Actor){
	        if(traversals[actor]!=null)juggler.remove(traversals[actor]);
        }

}

class Actor extends Sprite implements IAnimatable{
    
    public var skin_palette = [0xe8cda8, 0xffcc99, 0xfeb186, 0xdfc183];
    public var eyes_palette = [0x7bcbd5, 0x63ca70, 0xb4ac6e, 0x4b8ee7];
    public var hair_palette = [0xe6e67a, 0x212112, 0x504607, 0xc7c7c6];
    public var pants_palette = [0x4e3b3b, 0x570505, 0x120557, 0x11380a, 0x60470b];
    public var shirt_palette = [0x32869a, 0x4a8255, 0x832825, 0xa89594, 0x2f1818];
    public var shoes_palette = [0x1e1e15, 0x4b3c07, 0xa5a297];
    
    public var action:String;
    
    public var skin:Image;
    public var eyes:Image;
    public var hair:Image;
    public var mouth:Image;
    public var pants:Image;
    public var shirt:Image;
    public var shoes:Image;
    
    public var walking_skin:MovieClip;
    public var walking_eyes:MovieClip;
    public var walking_hair:MovieClip;
    public var walking_pants:MovieClip;
    public var walking_shirt:MovieClip;
    public var walking_shoes:MovieClip;
    
    public var rowing_skin:MovieClip;
    public var rowing_eyes:MovieClip;
    public var rowing_hair:MovieClip;
    public var rowing_pants:MovieClip;
    public var rowing_shirt:MovieClip;
    public var rowing_shoes:MovieClip;
    
    public var eating_skin:MovieClip;
    public var eating_eyes:MovieClip;
    public var eating_hair:MovieClip;
    public var eating_pants:MovieClip;
    public var eating_shirt:MovieClip;
    public var eating_shoes:MovieClip;
    public var eating_mouth:MovieClip;
    
    public var resting_skin:MovieClip;
    public var resting_eyes:MovieClip;
    public var resting_hair:MovieClip;
    public var resting_pants:MovieClip;
    public var resting_shirt:MovieClip;
    public var resting_shoes:MovieClip;
    public var resting_mouth:MovieClip;
    
    public var juggler:Juggler;
    
	public function new(){
        super();
        
        this.skin = new Image(Root.assets.getTexture("male_skin_1"));
        skin.color = skin_palette[Std.random(skin_palette.length)];
        skin.scaleX = 3;
        skin.scaleY = 3;
        skin.smoothing = TextureSmoothing.NONE;
        skin.readjustSize();
        this.eyes = new Image(Root.assets.getTexture("male_eyes_1"));
        eyes.color = eyes_palette[Std.random(eyes_palette.length)];
        eyes.scaleX = 3;
        eyes.scaleY = 3;
        eyes.smoothing = TextureSmoothing.NONE;
        eyes.readjustSize();
        this.hair = new Image(Root.assets.getTexture("male_hair_1"));
        hair.color = hair_palette[Std.random(hair_palette.length)];
        hair.scaleX = 3;
        hair.scaleY = 3;
        hair.smoothing = TextureSmoothing.NONE;
        hair.readjustSize();
        this.mouth = new Image(Root.assets.getTexture("male_mouth_1"));
        mouth.scaleX = 3;
        mouth.scaleY = 3;
        mouth.smoothing = TextureSmoothing.NONE;
        mouth.readjustSize();
        this.pants = new Image(Root.assets.getTexture("male_pants_1"));
        pants.color = pants_palette[Std.random(pants_palette.length)];
        pants.scaleX = 3;
        pants.scaleY = 3;
        pants.smoothing = TextureSmoothing.NONE;
        pants.readjustSize();
        this.shirt = new Image(Root.assets.getTexture("male_shirt_1"));
        shirt.color = shirt_palette[Std.random(shirt_palette.length)];
        shirt.scaleX = 3;
        shirt.scaleY = 3;
        shirt.smoothing = TextureSmoothing.NONE;
        shirt.readjustSize();
        this.shoes = new Image(Root.assets.getTexture("male_shoes_1"));
        shoes.color = shoes_palette[Std.random(shoes_palette.length)];
        shoes.scaleX = 3;
        shoes.scaleY = 3;
        shoes.smoothing = TextureSmoothing.NONE;
        shoes.readjustSize();
        
        this.walking_skin = new MovieClip(Root.assets.getTextures("male_skin_walking_"), 6);
        walking_skin.color = skin.color;
        walking_skin.scaleX = 3;
        walking_skin.scaleY = 3;
        walking_skin.smoothing = TextureSmoothing.NONE;
        walking_skin.readjustSize();
        this.walking_eyes = new MovieClip(Root.assets.getTextures("male_eyes_walking_"), 6);
        walking_eyes.color = eyes.color;
        walking_eyes.scaleX = 3;
        walking_eyes.scaleY = 3;
        walking_eyes.smoothing = TextureSmoothing.NONE;
        walking_eyes.readjustSize();
        this.walking_hair = new MovieClip(Root.assets.getTextures("male_hair_walking_"), 6);    
        walking_hair.color = hair.color;
        walking_hair.scaleX = 3;
        walking_hair.scaleY = 3;
        walking_hair.smoothing = TextureSmoothing.NONE;
        walking_hair.readjustSize();
        this.walking_pants = new MovieClip(Root.assets.getTextures("male_pants_walking_"), 6);
        walking_pants.color = pants.color;
        walking_pants.scaleX = 3;
        walking_pants.scaleY = 3;
        walking_pants.smoothing = TextureSmoothing.NONE;
        walking_pants.readjustSize();
        this.walking_shirt = new MovieClip(Root.assets.getTextures("male_shirt_walking_"), 6);
        walking_shirt.color = shirt.color;
        walking_shirt.scaleX = 3;
        walking_shirt.scaleY = 3;
        walking_shirt.smoothing = TextureSmoothing.NONE;
        walking_shirt.readjustSize();
        this.walking_shoes = new MovieClip(Root.assets.getTextures("male_shoes_walking_"), 6);
        walking_shoes.color = shoes.color;
        walking_shoes.scaleX = 3;
        walking_shoes.scaleY = 3;
        walking_shoes.smoothing = TextureSmoothing.NONE;
        walking_shoes.readjustSize();
        
        this.rowing_skin = new MovieClip(Root.assets.getTextures("male_skin_rowing_"), .635);
        rowing_skin.color = skin.color;
        rowing_skin.scaleX = 3;
        rowing_skin.scaleY = 3;
        rowing_skin.smoothing = TextureSmoothing.NONE;
        rowing_skin.readjustSize();
        addChild(rowing_skin);
        this.rowing_eyes = new MovieClip(Root.assets.getTextures("male_eyes_rowing_"), .635);
        rowing_eyes.color = eyes.color;
        rowing_eyes.scaleX = 3;
        rowing_eyes.scaleY = 3;
        rowing_eyes.smoothing = TextureSmoothing.NONE;
        rowing_eyes.readjustSize();
        addChild(rowing_eyes);
        this.rowing_hair = new MovieClip(Root.assets.getTextures("male_hair_rowing_"), .635);
        rowing_hair.color = hair.color;
        rowing_hair.scaleX = 3;
        rowing_hair.scaleY = 3;
        rowing_hair.smoothing = TextureSmoothing.NONE;
        rowing_hair.readjustSize();
        addChild(rowing_hair);
        this.rowing_pants = new MovieClip(Root.assets.getTextures("male_pants_rowing_"), .635);
        rowing_pants.color = pants.color;
        rowing_pants.scaleX = 3;
        rowing_pants.scaleY = 3;
        rowing_pants.smoothing = TextureSmoothing.NONE;
        rowing_pants.readjustSize();
        addChild(rowing_pants);
        this.rowing_shirt = new MovieClip(Root.assets.getTextures("male_shirt_rowing_"), .635);
        rowing_shirt.color = shirt.color;
        rowing_shirt.scaleX = 3;
        rowing_shirt.scaleY = 3;
        rowing_shirt.smoothing = TextureSmoothing.NONE;
        rowing_shirt.readjustSize();
        addChild(rowing_shirt);
        this.rowing_shoes = new MovieClip(Root.assets.getTextures("male_shoes_rowing_"), .635);
        rowing_shoes.color = shoes.color;
        rowing_shoes.scaleX = 3;
        rowing_shoes.scaleY = 3;
        rowing_shoes.smoothing = TextureSmoothing.NONE;
        rowing_shoes.readjustSize();
        addChild(rowing_shoes);
        juggler = new Juggler();
        juggler.add(rowing_skin);
        juggler.add(rowing_eyes);
        juggler.add(rowing_hair);
        juggler.add(rowing_pants);
        juggler.add(rowing_shirt);
        juggler.add(rowing_shoes);
        action = "rowing";
        
        this.eating_skin = new MovieClip(Root.assets.getTextures("male_skin_eating_"), 5);
        eating_skin.color = skin.color;
        eating_skin.scaleX = 3;
        eating_skin.scaleY = 3;
        eating_skin.smoothing = TextureSmoothing.NONE;
        eating_skin.readjustSize();
        this.eating_eyes = new MovieClip(Root.assets.getTextures("male_eyes_eating_"), 5);
        eating_eyes.color = eyes.color;
        eating_eyes.scaleX = 3;
        eating_eyes.scaleY = 3;
        eating_eyes.smoothing = TextureSmoothing.NONE;
        eating_eyes.readjustSize();
        this.eating_hair = new MovieClip(Root.assets.getTextures("male_hair_eating_"), 5);
        eating_hair.color = hair.color;
        eating_hair.scaleX = 3;
        eating_hair.scaleY = 3;
        eating_hair.smoothing = TextureSmoothing.NONE;
        eating_hair.readjustSize();
        this.eating_pants = new MovieClip(Root.assets.getTextures("male_pants_eating_"), 5);
        eating_pants.color = pants.color;
        eating_pants.scaleX = 3;
        eating_pants.scaleY = 3;
        eating_pants.smoothing = TextureSmoothing.NONE;
        eating_pants.readjustSize();
        this.eating_shirt = new MovieClip(Root.assets.getTextures("male_shirt_eating_"), 5);
        eating_shirt.color = shirt.color;
        eating_shirt.scaleX = 3;
        eating_shirt.scaleY = 3;
        eating_shirt.smoothing = TextureSmoothing.NONE;
        eating_shirt.readjustSize();
        this.eating_shoes = new MovieClip(Root.assets.getTextures("male_shoes_eating_"), 5);
        eating_shoes.color = shoes.color;
        eating_shoes.scaleX = 3;
        eating_shoes.scaleY = 3;
        eating_shoes.smoothing = TextureSmoothing.NONE;
        eating_shoes.readjustSize();
        this.eating_mouth = new MovieClip(Root.assets.getTextures("male_mouth_eating_"), 5);
        eating_mouth.color = mouth.color;
        eating_mouth.scaleX = 3;
        eating_mouth.scaleY = 3;
        eating_mouth.smoothing = TextureSmoothing.NONE;
        eating_mouth.readjustSize();
        
        this.resting_skin = new MovieClip(Root.assets.getTextures("male_skin_resting_"), 5);
        resting_skin.color = skin.color;
        resting_skin.scaleX = 3;
        resting_skin.scaleY = 3;
        resting_skin.smoothing = TextureSmoothing.NONE;
        resting_skin.readjustSize();
        this.resting_eyes = new MovieClip(Root.assets.getTextures("male_eyes_resting_"), 5);
        resting_eyes.color = skin.color;
        resting_eyes.scaleX = 3;
        resting_eyes.scaleY = 3;
        resting_eyes.smoothing = TextureSmoothing.NONE;
        resting_eyes.readjustSize();
        this.resting_hair = new MovieClip(Root.assets.getTextures("male_hair_resting_"), 5);
        resting_hair.color = hair.color;
        resting_hair.scaleX = 3;
        resting_hair.scaleY = 3;
        resting_hair.smoothing = TextureSmoothing.NONE;
        resting_hair.readjustSize();
        this.resting_pants = new MovieClip(Root.assets.getTextures("male_pants_resting_"), 5);
        resting_pants.color = pants.color;
        resting_pants.scaleX = 3;
        resting_pants.scaleY = 3;
        resting_pants.smoothing = TextureSmoothing.NONE;
        resting_pants.readjustSize();
        this.resting_shirt = new MovieClip(Root.assets.getTextures("male_shirt_resting_"), 5);
        resting_shirt.color = shirt.color;
        resting_shirt.scaleX = 3;
        resting_shirt.scaleY = 3;
        resting_shirt.smoothing = TextureSmoothing.NONE;
        resting_shirt.readjustSize();
        this.resting_shoes = new MovieClip(Root.assets.getTextures("male_shoes_resting_"), 5);
        resting_shoes.color = shoes.color;
        resting_shoes.scaleX = 3;
        resting_shoes.scaleY = 3;
        resting_shoes.smoothing = TextureSmoothing.NONE;
        resting_shoes.readjustSize();
        this.resting_mouth = new MovieClip(Root.assets.getTextures("male_mouth_resting_"), 5);
        resting_mouth.color = mouth.color;
        resting_mouth.scaleX = 3;
        resting_mouth.scaleY = 3;
        resting_mouth.smoothing = TextureSmoothing.NONE;
        resting_mouth.readjustSize();
    }
    
    public function stand(){
        removeChild(walking_skin);
        removeChild(walking_eyes);
        removeChild(walking_hair);
        removeChild(walking_pants);
        removeChild(walking_shirt);
        removeChild(walking_shoes);
        juggler.remove(walking_skin);
        juggler.remove(walking_eyes);
        juggler.remove(walking_hair);
        juggler.remove(walking_pants);
        juggler.remove(walking_shirt);
        juggler.remove(walking_shoes);
        addChild(skin);
        addChild(eyes);
        addChild(hair);
        addChild(mouth);
        addChild(pants);
        addChild(shirt);
        addChild(shoes);
        action = "standing";
    }
    
    public function walk(){
        if (action == "standing"){
            removeChild(skin);
            removeChild(eyes);
            removeChild(hair);
            removeChild(mouth);
            removeChild(pants);
            removeChild(shirt);
            removeChild(shoes);
        } else if (action == "rowing"){
            removeChild(rowing_skin);
            removeChild(rowing_eyes);
            removeChild(rowing_hair);
            removeChild(rowing_pants);
            removeChild(rowing_shirt);
            removeChild(rowing_shoes);
            juggler.remove(rowing_skin);
            juggler.remove(rowing_eyes);
            juggler.remove(rowing_hair);
            juggler.remove(rowing_pants);
            juggler.remove(rowing_shirt);
            juggler.remove(rowing_shoes);
            this.scaleX = .5;
        } else if (action == "eating"){
            removeChild(eating_skin);
            removeChild(eating_eyes);
            removeChild(eating_hair);
            removeChild(eating_pants);
            removeChild(eating_shirt);
            removeChild(eating_shoes);
            removeChild(eating_mouth);
            juggler.remove(eating_skin);
            juggler.remove(eating_eyes);
            juggler.remove(eating_hair);
            juggler.remove(eating_pants);
            juggler.remove(eating_shirt);
            juggler.remove(eating_shoes);
            juggler.remove(eating_mouth);
        } else if (action == "resting"){
            removeChild(resting_skin);
            removeChild(resting_eyes);
            removeChild(resting_hair);
            removeChild(resting_pants);
            removeChild(resting_shirt);
            removeChild(resting_shoes);
            removeChild(resting_mouth);
            juggler.remove(resting_skin);
            juggler.remove(resting_eyes);
            juggler.remove(resting_hair);
            juggler.remove(resting_pants);
            juggler.remove(resting_shirt);
            juggler.remove(resting_shoes);
            juggler.remove(resting_mouth);
        }
        addChild(walking_skin);
        addChild(walking_eyes);
        addChild(walking_hair);
        addChild(walking_pants);
        addChild(walking_shirt);
        addChild(walking_shoes);
        juggler.add(walking_skin);
        juggler.add(walking_eyes);
        juggler.add(walking_hair);
        juggler.add(walking_pants);
        juggler.add(walking_shirt);
        juggler.add(walking_shoes);
        action = "walking";
    }
    
    public function row(){
        removeChild(walking_skin);
        removeChild(walking_eyes);
        removeChild(walking_hair);
        removeChild(walking_pants);
        removeChild(walking_shirt);
        removeChild(walking_shoes);
        juggler.remove(walking_skin);
        juggler.remove(walking_eyes);
        juggler.remove(walking_hair);
        juggler.remove(walking_pants);
        juggler.remove(walking_shirt);
        juggler.remove(walking_shoes);
        this.scaleX = -.5;
        addChild(rowing_skin);
        addChild(rowing_eyes);
        addChild(rowing_hair);
        addChild(rowing_pants);
        addChild(rowing_shirt);
        addChild(rowing_shoes);
        juggler.add(rowing_skin);
        juggler.add(rowing_eyes);
        juggler.add(rowing_hair);
        juggler.add(rowing_pants);
        juggler.add(rowing_shirt);
        juggler.add(rowing_shoes);
        action = "rowing";
    }
    
    public function eat(){
        removeChild(walking_skin);
        removeChild(walking_eyes);
        removeChild(walking_hair);
        removeChild(walking_pants);
        removeChild(walking_shirt);
        removeChild(walking_shoes);
        juggler.remove(walking_skin);
        juggler.remove(walking_eyes);
        juggler.remove(walking_hair);
        juggler.remove(walking_pants);
        juggler.remove(walking_shirt);
        juggler.remove(walking_shoes);
        addChild(eating_skin);
        addChild(eating_eyes);
        addChild(eating_hair);
        addChild(eating_pants);
        addChild(eating_shirt);
        addChild(eating_shoes);
        addChild(eating_mouth);
        juggler.add(eating_skin);
        juggler.add(eating_eyes);
        juggler.add(eating_hair);
        juggler.add(eating_pants);
        juggler.add(eating_shirt);
        juggler.add(eating_shoes);
        juggler.add(eating_mouth);
        action = "eating";
    }
    
    public function resting(){
        removeChild(walking_skin);
        removeChild(walking_eyes);
        removeChild(walking_hair);
        removeChild(walking_pants);
        removeChild(walking_shirt);
        removeChild(walking_shoes);
        juggler.remove(walking_skin);
        juggler.remove(walking_eyes);
        juggler.remove(walking_hair);
        juggler.remove(walking_pants);
        juggler.remove(walking_shirt);
        juggler.remove(walking_shoes);
        addChild(resting_skin);
        addChild(resting_eyes);
        addChild(resting_hair);
        addChild(resting_pants);
        addChild(resting_shirt);
        addChild(resting_shoes);
        addChild(resting_mouth);
        juggler.add(resting_skin);
        juggler.add(resting_eyes);
        juggler.add(resting_hair);
        juggler.add(resting_pants);
        juggler.add(resting_shirt);
        juggler.add(resting_shoes);
        juggler.add(resting_mouth);
        action = "resting";
    }
    
    public function advanceTime(t:Float){
        juggler.advanceTime(t);
    }
}
 
class Traversal implements IAnimatable{
	public var overwatch:Overwatch;
	public var destination_node:Node;
	public var actor:Actor;
	public var x_origin:Float;
	public var y_origin:Float;
	public var x_distance:Float;
	public var y_distance:Float;
	public var duration:Float;
	public var time:Float;
	public var onComplete:Void->Void;

	public function new(overwatch:Overwatch, actor:Actor, destination_node:Node, onComplete:Void->Void){
		this.overwatch = overwatch;
		this.destination_node = destination_node;
		this.actor = actor;
		this.x_origin = actor.x;
		this.y_origin = actor.y;
		this.x_distance = destination_node.getX()-actor.x;
		this.y_distance = destination_node.getY()-actor.y;
		this.duration = Math.sqrt( Math.pow(destination_node.getX()-actor.x, 2)+Math.pow(destination_node.getY()-actor.y, 2) )/160;
		this.onComplete = onComplete;
		this.time = 0;

		actor.scaleX=(x_distance<0)?-.5:.5;
		overwatch.actors[actor]=destination_node;
		destination_node.assignActor(actor);
	}

	public function advanceTime(t:Float){
		//Can do animated stuff here instead.
		time+=t;

		if(t==-1 || time>=duration){
			actor.x=destination_node.getX();
			actor.y=destination_node.getY();
			done();
		}
		else{
			actor.x = x_origin+((x_distance)*(time/duration));
			actor.y = y_origin+((y_distance)*(time/duration));
		}
	}

	private function done(){
		actor.scaleX=.5;
		overwatch.juggler.remove(this);
		if(onComplete!=null){onComplete();}
	}
}

class Node{
	private var x:Float;
	private var y:Float;
	private var actor_xOffset:Float;
	private var actor_yOffset:Float;
	private var contains:Array<Actor>;

	//Local x,y to the Overwatch Sprite
	public function new(x,y,actor_xOffset=0,actor_yOffset=0){
		contains = new Array<Actor>();
		this.x = x; 
		this.y = y;
		this.actor_xOffset = actor_xOffset;
		this.actor_yOffset = actor_yOffset;
	}

	public function assignActor(actor){
		if(contains.indexOf(actor)==-1){contains.push(actor);return true;}
		return false;
	}
	public function removeActor(actor){
		if(contains.remove(actor)){update();return true;}
		return false;
	}

	private function update(){
		for(i in 0...contains.length){
			var actor = contains[i];
			actor.x=(x+(actor_xOffset*i));
			actor.y=(y+(actor_yOffset*i));
		}
	}

	public function getX(){return x+(actor_xOffset*contains.length);}
	public function getY(){return y+(actor_yOffset*contains.length);}

        public function containsActor(actor){
            return (contains.indexOf(actor)==-1)?false:true;
        }

        public function isEmpty(){
            //trace(contains);
            return contains.length == 0;
        }
}
