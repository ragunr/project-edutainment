import starling.display.Sprite;
import starling.core.Starling;
import Dialogue;

using Lambda;

class ToJarrowScene extends Sprite {
	
	public var onComplete:Void -> Void;

	public function new() {
		super();

		var d1 = new Dialogue(
			"Captain",
			["An ENTIRE YEAR has passed...",
		     "We head again towards another English...",
			 "Ahem, I mean Northumbrian monastery...",
			 "Onward to Jarrow!"].list());

		Starling.juggler.delayCall(function() {
            Starling.juggler.add(d1); 
			}, 0.75);
		addChild(d1);
       
		d1.onComplete = function() { this.onComplete(); };

	}	

}
