import starling.display.Sprite;
import starling.display.Quad;
import starling.display.Image;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;
import starling.animation.*;
import starling.text.*;
import starling.events.*;
import starling.utils.HAlign;
import starling.utils.VAlign;
import starling.core.Starling;

using Lambda;


class Dialogue extends Sprite implements IAnimatable {
    public static var HEIGHT_PER_LINE = 40;
    public static var CHAR_DELAY = 0.025;
    public static var POPUP_DELTA_Y = 200;

    public var fwidth:Float;
    public var fheight:Float;
    public var title:String;
    public var text_queue:List<String>;
    public var box:DialogueBox;

    public var onComplete:Void->Void;

    private var next_arrow:Image;
    private var anim_juggler:Juggler;
    private var text_juggler:Juggler;
    private var phase:DialoguePhase;

    private var flash_dc:DelayedCall;

    public function new(
            title:String,
            text_queue:List<String>,
            ?avatar:Quad)
    {
        this.fwidth = 930;
        var lines = 3;
        this.fheight = lines*HEIGHT_PER_LINE;
        this.title = title;
        this.text_queue = text_queue;
        super();

        x = 50;
        y = 436;

        anim_juggler = new Juggler();
        text_juggler = new Juggler();

        box = new DialogueBox(fwidth,fheight, title, avatar);
        //next_arrow = new Quad(40,40,0xFF00FF);
        next_arrow = new Image(Root.assets.getTexture("chevrons"));
        next_arrow.scaleX = next_arrow.scaleY = 2;
        next_arrow.smoothing = TextureSmoothing.NONE;
        next_arrow.x = Std.int(fwidth - 50);
        next_arrow.y = fheight - 25;
        next_arrow.alpha = 0;


        addChild(box);
        addChild(next_arrow);

        popup();
        //rollout_text();

        addEventListener(TouchEvent.TOUCH, function(event:TouchEvent){
                var touch = event.getTouch(this,TouchPhase.BEGAN);
                if(touch != null) onInput(event);
                });

        addEventListener(KeyboardEvent.KEY_DOWN, 
                function(e:Event){onInput(e);});

    }

    public function advanceTime(time:Float){
        anim_juggler.advanceTime(time);
        text_juggler.advanceTime(time);
    }

    public function onInput(e:Event){
        if(phase == WRITE){
            text_juggler.advanceTime(30);
        }
        else if(phase == WAIT){
            anim_juggler.remove(flash_dc);
            if(text_queue.length > 0) rollout_text();
            else popdown();
        }
    }

    private function popup(){
        phase = OPEN;
        box.y += POPUP_DELTA_Y;
        var popup_tween = new Tween(box,0.125,Transitions.LINEAR);
        popup_tween.animate("y",0);
        anim_juggler.add(popup_tween);

        popup_tween.onComplete = function(){
            rollout_text();
        };
    }

    private function popdown(){
        phase = OPEN;
        var popup_tween = new Tween(box,0.125,Transitions.LINEAR);
        popup_tween.animate("y",POPUP_DELTA_Y);
        anim_juggler.add(popup_tween);

        popup_tween.onComplete = function(){
            done();
        };
    }

    private function rollout_text(){
        phase = WRITE;
        var text = text_queue.pop();
        box.set_text(title);

        for(i in 0...text.length){
            text_juggler.delayCall(function (){
                    box.set_text(title+"\n"+text.substr(0,i+1));
                    }, i * CHAR_DELAY);
        }

        text_juggler.delayCall(function(){
                wait();
                },text.length * CHAR_DELAY);
    }

    private function wait(){
        phase = WAIT;
        if(text_queue.length > 0){
            next_arrow.alpha = 1;
            flash_dc = new DelayedCall(function(){
                    next_arrow.alpha = 1-next_arrow.alpha;
                    },0.5);
            flash_dc.repeatCount = 0;
            anim_juggler.add(flash_dc);
        }
    }

    private function done(){
        if(onComplete != null) onComplete();
    }
}

enum DialoguePhase{
    OPEN;
    WRITE;
    WAIT;
    CLOSE;
}

class DialogueBox extends Sprite {
    public static var BORDER_SIZE = 4;
    public static var TEXT_PADDING = 8;
    public static var AVATAR_PADDING = 4;


    public var fwidth:Float;
    public var fheight:Float;
    public var text_field:TextField;

    public function new(
            fwidth:Float,
            fheight:Float,
            initial_text:String,
            ?avatar:Quad)
    {
        this.fwidth = fwidth;
        this.fheight = fheight;

        super();

        var avatar_width = 0.0;
        if(avatar != null) avatar_width = avatar.width;

        var border = new Quad(fwidth,fheight,0x000000);
        var body = new Quad(
                fwidth-BORDER_SIZE*2,
                fheight-BORDER_SIZE*2,
                0x4444FF);
        body.setVertexColor(0,0x8f563b);
        body.setVertexColor(1,0x8f563b);
        body.setVertexColor(2,0x663931);
        body.setVertexColor(3,0x663931);
        body.x = BORDER_SIZE;
        body.y = BORDER_SIZE;

        var frame = new Image(Root.assets.getTexture("axeframe"));
        frame.scaleX = frame.scaleY = 2;
        frame.smoothing = TextureSmoothing.NONE;
        frame.x = -59;
        frame.y = -10;

        text_field = new TextField(
                Std.int(fwidth-(BORDER_SIZE+TEXT_PADDING)*2-avatar_width),
                Std.int(fheight-(BORDER_SIZE+TEXT_PADDING)*2),
                initial_text,
                "Gameboy_32_0",
                32,
                0xFFFFFF
                );
        text_field.hAlign = HAlign.LEFT;
        text_field.vAlign = VAlign.TOP;
        text_field.x = BORDER_SIZE+avatar_width+AVATAR_PADDING+TEXT_PADDING;
        text_field.y = BORDER_SIZE+TEXT_PADDING;

        if(avatar != null){
            avatar.x = BORDER_SIZE+AVATAR_PADDING;
            avatar.y = fheight-(BORDER_SIZE+TEXT_PADDING)-avatar.height;
        }
        
        addChild(border);
        addChild(body);
        addChild(frame);
        if(avatar != null) addChild(avatar);
        addChild(text_field);
    }

    public function set_text(text:String){
        text_field.text = text;
    }
}
class DialogueTest extends Sprite {

    // We can't have an image in multiple places, so if we want to
    // make dialogues up front, we will have to generate an avatar
    // for each call.
    public function pirate_avatar(){
        var img = new Image(Root.assets.getTexture("pc_pirate"));
        img.smoothing=TextureSmoothing.NONE;
        img.readjustSize();
        return img;
    }

    public function new(){
        super();

        var d1 = new Dialogue(
                "test",
                ["\"abcdefghijklmnopqrstuvwxyz"+
                "abcdefghijklmnopqrstuvwxyz"+
                "abcdefghijklmnopq.\"",
                "zyxwvutsrqponmlkjihgfedcba"].list());

        var d2 = new Dialogue(
                "Pirate",
                ["\"Say what?\""].list(),
                pirate_avatar());

        var d3 = new Dialogue(
                "test",
                ["\"AAAAABBBBBCCCCDDDDEEEEEFFFFFGGGGGHHHHHIIIIJJJ\"",
                "\"KKKKKLLLLLMMMMMMMMNNNNNNNNNOOOOOOOPPPPPPQQQQQQ\"",
                "\"RRRRRRRSSSSSSTTTTTTUUUUUVVVVVWWWWWXXXXXXYYYYZZ\""].list());

        var d4 = new Dialogue(
                "Pirate",
                ["\"Stop that. You're freaking me out.\""].list(),
                pirate_avatar());

        var d5 = new Dialogue(
                "test",
                ["~!@#$%^&*()_+1234567890-=qwertyuiop[]\\asdfghjkl;'zxcfvgbhnjm,./"].list());

        var d6 = new Dialogue(
                "Pirate",
                ["Right then. I'm out."].list(),
                pirate_avatar());
        

        Starling.juggler.delayCall(function (){
                Starling.juggler.add(d1);
            },2.0);
        addChild(d1);

        d1.onComplete = function(){
            removeChild(d1);
            Starling.juggler.remove(d1);
            addChild(d2);
            Starling.juggler.add(d2);
        }

        d2.onComplete = function(){
            removeChild(d2);
            Starling.juggler.remove(d2);
            addChild(d3);
            Starling.juggler.add(d3);
        }

        d3.onComplete = function(){
            removeChild(d3);
            Starling.juggler.remove(d3);
            addChild(d4);
            Starling.juggler.add(d4);
        }

        d4.onComplete = function(){
            removeChild(d4);
            Starling.juggler.remove(d4);
            addChild(d5);
            Starling.juggler.add(d5);
        }

        d5.onComplete = function(){
            removeChild(d5);
            Starling.juggler.remove(d5);
            addChild(d6);
            Starling.juggler.add(d6);
        }

        d6.onComplete = function(){
            removeChild(d6);
            Starling.juggler.remove(d6);
            trace("done");
        }

        
    }
}
