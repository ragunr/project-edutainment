import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.utils.*;
import starling.textures.*;
import starling.text.*;
import Game;



class Hud extends Sprite {
    var game : Game;
    private var hud_layer : Sprite;
    private var meal_count:TextField;
    private var ship_progress:TextField;
    private var ship_speed:TextField;
    private var char_name:TextField;
    private var char_sleep:Quad;
    private var char_hunger:Quad;

    public function new(game:Game){
        super();
        this.game = game;

        //Add Hud Layer
        hud_layer = new Sprite();

        //Setup Hud Elements
        var ship_hud = new Image(Root.assets.getTexture("ship_hud"));
		ship_hud.scaleX = ship_hud.scaleY = 2;
        ship_hud.smoothing = TextureSmoothing.NONE;
        ship_hud.readjustSize();
        ship_hud.x = Root.WIDTH-5-ship_hud.width;
        ship_hud.y = 5;

        ship_progress = new TextField(58, 35, "", "Gameboy_32_0", 16);
        ship_progress.x = ship_hud.x+124;
        ship_progress.y = ship_hud.y+60;
        ship_progress.hAlign = HAlign.CENTER;
        ship_progress.vAlign = VAlign.CENTER;

        ship_speed = new TextField(58, 35, "", "Gameboy_32_0", 16);
        ship_speed.x = ship_hud.x+124;
        ship_speed.y = ship_hud.y+116;
        ship_speed.hAlign = HAlign.CENTER;
        ship_speed.vAlign = VAlign.CENTER;

        meal_count = new TextField(58, 35, "", "Gameboy_32_0", 16);
        meal_count.x = ship_hud.x+124;
        meal_count.y = ship_hud.y+176;
        meal_count.hAlign = HAlign.CENTER;
        meal_count.vAlign = VAlign.CENTER;

        var char_hud = new Image(Root.assets.getTexture("char_hud"));
        char_hud.scaleX = char_hud.scaleY = 2;
        char_hud.smoothing = TextureSmoothing.NONE;
        char_hud.x = 5;
        char_hud.y = 5;

        char_name = new TextField(88, 32, "", "Gameboy_32_0", 16);
        char_name.x = char_hud.x+75;
        char_name.y = char_hud.y+57;
        char_name.hAlign = HAlign.CENTER;
        char_name.vAlign = VAlign.CENTER;

        char_hunger = new Quad(72,33,0x99E550);
        char_hunger.scaleX = 0;
        char_hunger.x=char_hud.x+110;
        char_hunger.y=char_hud.y+108;

        char_sleep = new Quad(72,34,0x4040C0);
        char_sleep.scaleX = 0;
        char_sleep.x=char_hud.x+110;
        char_sleep.y=char_hud.y+166;


        hud_layer.addChild(ship_hud);
        hud_layer.addChild(char_hud);
        hud_layer.addChild(ship_progress);
        hud_layer.addChild(ship_speed);
        hud_layer.addChild(meal_count);
        hud_layer.addChild(char_name);
        hud_layer.addChild(char_hunger);
        hud_layer.addChild(char_sleep);
        hud_layer.touchable = false;

        addChild(hud_layer);
    }

    public function setMealCount(meal_count:Int){this.meal_count.text=(meal_count+"");}
    public function setShipSpeed(ship_speed:Float){this.ship_speed.text=(((ship_speed*6.25)+"").substring(0, 4));}
    public function setShipProgress(ship_progress:Float){this.ship_progress.text=(ship_progress+"").substring(0,4);}
    public function updateCharacter(crewman:Crewman){
    	char_name.text = crewman.name;
    	char_hunger.scaleX = 1-crewman.stats.get(HUNGER);
    	char_sleep.scaleX = 1-crewman.stats.get(FATIGUE);
    }
}
