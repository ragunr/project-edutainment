import starling.display.Sprite;
import starling.utils.AssetManager;
import starling.display.Image;
import starling.core.Starling;
import starling.animation.Transitions;
import starling.display.Quad;
import Game;
import Dialogue;
import Overwatch;

class Root extends Sprite {

    public static var assets:AssetManager;
    public static inline var WIDTH = 1024;
    public static inline var HEIGHT = 576;

    public function new() {
        super();
    }
    public function initialize(startup:Startup) {
        assets = new AssetManager();
        // placeholders
        //assets.enqueue("assets/pc_pirate.png");
        // enqueue here
        assets.enqueue("assets/assets.png");
        assets.enqueue("assets/assets.xml");
        assets.enqueue("assets/Gameboy_32.fnt");
        assets.enqueue("assets/Gameboy_64.fnt");

        assets.enqueue("assets/odin.mp3");
        assets.enqueue("assets/viking.mp3");
        assets.enqueue("assets/ocean.mp3");
        assets.enqueue("assets/die.mp3");

        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup.loadingBitmap,
                    1.0,
                    {
                        transition: Transitions.EASE_OUT,
                        delay: 1.0,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
                            startup.removeChild(startup.loadingBg);
                        }
                    });
                start();
                //dialogue_test();
                //move_test();
                //game_test();
            }
        });
    }

    public function start(){
		removeChildren();
        var menu:Menu = new Menu(this);
		addChild(menu);
    }

    public function game_test(){
        var game = new Game(this);
        addChild(game);
        Starling.juggler.add(game);
    }

	public function reset(){
		start();
	}

    public function dialogue_test(){
        var test = new DialogueTest();
        addChild(test);
    }

    public function move_test(){
        var quad = new Actor();
        addChild(quad);
        var t_overwatch = new Overwatch();
        t_overwatch.addNode("Port", 100, 100);
        t_overwatch.addNode("Starboard", 300, 200);
        t_overwatch.queueTravelTo(quad, "Starboard", function(){trace("Completed");});
        t_overwatch.queueTravelTo(quad, "Port");

        t_overwatch.advanceTravels();
    }
}
